# Kafka consumers and producers

## Preparation
In the docker-compose.yml change the ip-adres to your host ipadres

In the consumer and the producer folder run
```
$ sh build_docker.sh
```

## Starting Kafka
From the root folder
```
$ docker-compose up -d
```

To stop the composition:
```
$ docker-compose down
```

## Running the consumer and producer
Now execute in a different shell:
```
$ docker network ls
```
You will notice there is a new network created <foldername>_default. In my case it is kafka_default.

Now run a consumer in a shell with:
```
$ docker run --rm --network="kafka_default" coilz/kafka-consumer kafka my-topic
```
You can also add it to the compose file if you want

And run a producer:
```
$ docker run --rm -it --network="kafka_default" coilz/kafka-producer kafka my-topic
```
