﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;

namespace consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            bool keepRunning = true;
            Console.CancelKeyPress += (_, e) => {
                e.Cancel = true; // prevent the process from terminating.
                keepRunning = false;
            };
            Console.WriteLine("Press Ctrl+C or Ctrl_Break to exit.");

            string brokerList = args[0];
            var topics = args.Skip(1).ToList();

            var config = new Dictionary<string, object>
            {
                { "group.id", "simple-csharp-consumer" },
                { "bootstrap.servers", brokerList }
            };

            using (var consumer = new Consumer<Null, string>(config, null, new StringDeserializer(Encoding.UTF8)))
            {
                var topicPartitionOffsets = new List<TopicPartitionOffset>();
                foreach (var topic in topics)
                {
                    var topicPartitionOffset = new TopicPartitionOffset(topic, 0, 0);
                    topicPartitionOffsets.Add(topicPartitionOffset);
                }

                consumer.Assign(topicPartitionOffsets);

                var topicsString = topics.Aggregate(string.Empty, (t, s) => string.Join(" #", t, s));
                Console.WriteLine($"{consumer.Name} consuming from {topicsString}.");

                while (keepRunning)
                {
                    Message<Null, string> msg;
                    if (consumer.Consume(out msg, TimeSpan.FromSeconds(1)))
                    {
                        Console.WriteLine($"Topic: {msg.Topic} Partition: {msg.Partition} Offset: {msg.Offset} {msg.Value}");
                    }
                }
            }
        }
    }
}
